<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

require dirname(__DIR__).'/vendor/autoload.php';

$fileLocator = new FileLocator(__DIR__ . '/../config/');

$containerBuilder = new ContainerBuilder();
$loader = new \Symfony\Component\DependencyInjection\Loader\YamlFileLoader(
	$containerBuilder,
	$fileLocator
);

$loader->load('services.yaml');
$containerBuilder->compile();

$controllerResolver = new ContainerControllerResolver($containerBuilder);

$argumentResolver = new ArgumentResolver();

$loader = new YamlFileLoader($fileLocator);
$routes = $loader->load(__DIR__ . '/../config/routes.yaml');
$matcher = new UrlMatcher($routes, new RequestContext());

$requestStack = new RequestStack();

$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new RouterListener($matcher, $requestStack));

$kernel = new HttpKernel(
	$dispatcher,
	$controllerResolver,
	$requestStack,
	$argumentResolver
);

$request = Request::createFromGlobals();

$response = $kernel->handle($request)->send();

$kernel->terminate($request, $response);
