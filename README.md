# TODO list API

## Spusteni systemu
 - `composer install`
 - `docker-compose up`

Funkcnost lze otestovat pomoci swaggera (localhost:81/api/swagger.html).

API je postaveno na dvou resourcech resp hlavnim resourcu `/lists/` a subresourcu `/tasks/`. Uvnitr aplikace jsou pak reprezentovany entitami `TaskList` a `Task`.

### Dostupne endpointy
host: localhost:81

`GET|POST /lists/`

`GET|PATCH|DELETE /lists/{id}/`

`GET|POST /lists/{id}/tasks/`

`GET|PATCH|DELETE /lists/{id}/tasks/{taskId}/`

## Architektura
### Pouzite technologie

 - PHP 8.0.3
 - MariaDB (moznost pristupu pres adminera **localhost:8081** (root:root))
 - Apache
 - Swagger (localhost:81/api/swagger.html)
 - Symfony 5 komponenty

### Struktura aplikace
Aplikace je rozdelena do 3 vrstev:

    infrastructure/controllers
               |
               V
           application
               |
               V
             domain

Na nejvyssi urovni je INFRA vrstva obsahujici adaptery (implementacni detaily mechanismu vyuzivanych aplikaci).
Jde o mechanismy, ktere nejsou primo spojeny s bisnis pozadavky, ale jsou zapotrebi k behu aplikace.
Podstatne je, aby aplikace na techto implementacich nebyla zavisla z duvodu jednoduche zamenitelnosti napr v pripade,
kdy knihovna prestane byt udrzovana, a to bez potreby zasahu do aplikacni logiky. Vyuziva se tedy principu otoceni zavislosti (DIP - dependency inversion principle).
Na urovni INFRA vrstvy jsou i `controllery`/`presentery`, ktere je mozne v pripade potreby zamenit napr za CLI command, RPC request atd.
Diky tomuto pristupu je tedy mozne provolat jakykoliv use-case odkudkoliv.

`Aplikacni vrstva` ridi proces konkretniho use-casu pomoci volani dilcich domenovych akci. Krome toho muze byt jeji zodpovednosti i transakcni provedeni procesu.

V `domenove vrstve` je pak veskera business logika na urovni entit/value-objektu (rich model) nebo v pripade potreby domenovych servis.

Podstatne je, aby aplikace nebyla zavisla na infrastrukture, coz muze byt pomerne slozite uhlidat. K tomuto existuje
sikovna knihovna Deptrac (https://github.com/qossmic/deptrac), kterou je mozne pridat do CI pipeliny.

## Controllery
Jak jsem zminil vyse, architektura je navrzena tak, aby controller bylo mozne jednoduse zamenit napr za CLI command a spoustet tak use-casy napr jako cron joby. Aktualni controllery jsou specificke pro Symfony. V tomto pripade ocekavaji na vstupu Symfony request a vraceji Symfony response. Pri prechodu na jiny FW by byla zapotrebi pouze nova implementace controlleru - jadro aplikace touto zmenou nebude ovlivneno.

Controllery jsou registrovany jako sluzby bez zavislosti na service lokatoru, coz usnadnuje/umoznuje testovani a jasne definuje jeho zavislosti. Bylo by na zvazeni pouziti vice specifickych invokable controlleru, cimz by se snizil pocet zavislosti a usnadnilo se tak i testovani.

### Databaze
Aktualne jsem pro pristup k DB pouzil PDO nicmene vzhledem k tomu, ze je domena a aplikacni vrstva nezavisla na konkretni implementaci, lze jednoduse pomoci DI vyuzit napr Doctrine implementaci.

## Testing
Aplikaci jsem pokryl z vetsi casti unit testy. Stalo by za to vytvorit i integracni testy, pokryvajici jednotlive use-casy pripadne end-to-end testy pokryvajici cele API.

## TODO
- autentifikace/autorizace pomoci tokenu
- kesovani (v aktualnim stavu hlavne routy a kontejner, ktery se zbytecne kompiluje pri kazdem spusteni)
- Zaveden ENV promennych
- DB transakce
- integracni testy
