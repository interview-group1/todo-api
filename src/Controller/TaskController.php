<?php

declare(strict_types = 1);

namespace Interview\Todo\Controller;

use Interview\Todo\Application\Task\CreateTaskUseCase;
use Interview\Todo\Application\Task\GetAllTasksUseCase;
use Interview\Todo\Application\Task\GetTaskUseCase;
use Interview\Todo\Application\Task\RemoveTaskUseCase;
use Interview\Todo\Application\Task\UpdateTaskUseCase;
use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskNotFoundException;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskController
{
	/**
	 * @var GetTaskUseCase
	 */
	private GetTaskUseCase $getTaskUseCase;

	/**
	 * @var CreateTaskUseCase
	 */
	private CreateTaskUseCase $createTaskUseCase;

	/**
	 * @var LoggerInterface
	 */
	private LoggerInterface $logger;

	/**
	 * @var RemoveTaskUseCase
	 */
	private RemoveTaskUseCase $removeTaskUseCase;

	/**
	 * @var UpdateTaskUseCase
	 */
	private UpdateTaskUseCase $updateTaskUseCase;

	/**
	 * @var GetAllTasksUseCase
	 */
	private GetAllTasksUseCase $getAllTaskUseCase;

	/**
	 * @param GetTaskUseCase $getTaskUseCase
	 * @param CreateTaskUseCase $createTaskUseCase
	 * @param RemoveTaskUseCase $removeTaskUseCase
	 * @param UpdateTaskUseCase $updateTaskUseCase
	 * @param GetAllTasksUseCase $getAllTaskUseCase
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		GetTaskUseCase $getTaskUseCase,
		CreateTaskUseCase $createTaskUseCase,
		RemoveTaskUseCase $removeTaskUseCase,
		UpdateTaskUseCase $updateTaskUseCase,
		GetAllTasksUseCase $getAllTaskUseCase,
		LoggerInterface $logger
	) {
		$this->getTaskUseCase = $getTaskUseCase;
		$this->createTaskUseCase = $createTaskUseCase;
		$this->removeTaskUseCase = $removeTaskUseCase;
		$this->updateTaskUseCase = $updateTaskUseCase;
		$this->getAllTaskUseCase = $getAllTaskUseCase;
		$this->logger = $logger;
	}

	/**
	 * @param Request $request
	 * @param string $id
	 * @param string|null $taskId
	 *
	 * @return Response
	 */
	public function get(Request $request, string $id, string $taskId = null): Response
	{
		$data = [];
		try
		{
			if (is_null($taskId))
			{
				$collection = $this->getAllTaskUseCase->execute(
					new TaskListId($id)
				);

				/** @var Task $task */
				foreach ($collection as $task)
				{
					$data[] = $this->transform($task);
				}
			}
			else
			{
				$task = $this->getTaskUseCase-> execute(
					new TaskListId($id),
					new TaskId($taskId)
				);

				$data = $this->transform($task);
			}
		}
		catch (TaskNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to get Task resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse(
			['data' => $data]
		);
	}

	/**
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function post(Request $request): Response
	{
		try
		{
			$id = $this->createTaskUseCase->execute(
				new TaskListId($request->get('id')),
				$request->get('name')
			);

			$this->logger->info('Task ' . $id->toString() . ' created.');
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to create Task resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse(['id' => $id->toString()]);
	}

	/**
	 * @param Request $request
	 * @param string $id
	 * @param string $taskId
	 *
	 * @return Response
	 */
	public function patch(Request $request, string $id, string $taskId): Response
	{
		try
		{
			$this->updateTaskUseCase->execute(
				new TaskListId($id),
				new TaskId($taskId),
				!$request->get('name') ? null : $request->get('name'),
				!$request->get('status') ? null : new TaskStatus($request->get('status'))
			);

			$this->logger->info('Task ' . $id . ' updated.');
		}
		catch (TaskNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to update Task resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse([], 204);
	}

	/**
	 * @param Request $request
	 * @param string $id
	 * @param string $taskId
	 *
	 * @return Response
	 */
	public function delete(Request $request, string $id, string $taskId): Response
	{
		try
		{
			$this->removeTaskUseCase->execute(
				new TaskListId($id),
				new TaskId($taskId),
			);

			$this->logger->info('Task ' . $id . ' deleted.');
		}
		catch (TaskNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to delete Task resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse([], 204);
	}

	/**
	 * @param Task $task
	 *
	 * @return array<string, mixed>
	 */
	private function transform(Task $task): array
	{
		return [
			'id' => $task->getId()->toString(),
			'name' => $task->getName(),
			'status' => $task->getStatus()->toString(),
			'listId' => $task->getTaskListId()->toString(),
			'createdAt' => $task->getCreatedAt()->format('Y-m-d H:i:s')
		];
	}
}
