<?php

declare(strict_types = 1);

namespace Interview\Todo\Controller;

use Interview\Todo\Application\TaskList\CreateTaskListUseCase;
use Interview\Todo\Application\TaskList\GetAllTaskListUseCase;
use Interview\Todo\Application\TaskList\GetTaskListUseCase;
use Interview\Todo\Application\TaskList\RemoveTaskListUseCase;
use Interview\Todo\Application\TaskList\UpdateTaskListUseCase;
use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskListController
{
	/**
	 * @var GetTaskListUseCase
	 */
	private GetTaskListUseCase $getListUseCase;

	/**
	 * @var CreateTaskListUseCase
	 */
	private CreateTaskListUseCase $createTaskListUseCase;

	/**
	 * @var RemoveTaskListUseCase
	 */
	private RemoveTaskListUseCase $removeTaskListUseCase;

	/**
	 * @var UpdateTaskListUseCase
	 */
	private UpdateTaskListUseCase $updateTaskListUseCase;

	/**
	 * @var GetAllTaskListUseCase
	 */
	private GetAllTaskListUseCase $getAllTaskListUseCase;

	/**
	 * @var LoggerInterface
	 */
	private LoggerInterface $logger;

	/**
	 * @param GetTaskListUseCase $getListUseCase
	 * @param CreateTaskListUseCase $createTaskListUseCase
	 * @param RemoveTaskListUseCase $removeTaskListUseCase
	 * @param UpdateTaskListUseCase $updateTaskListUseCase
	 * @param GetAllTaskListUseCase $getAllTaskListUseCase
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		GetTaskListUseCase $getListUseCase,
		CreateTaskListUseCase $createTaskListUseCase,
		RemoveTaskListUseCase $removeTaskListUseCase,
		UpdateTaskListUseCase $updateTaskListUseCase,
		GetAllTaskListUseCase $getAllTaskListUseCase,
		LoggerInterface $logger
	) {
		$this->getListUseCase = $getListUseCase;
		$this->createTaskListUseCase = $createTaskListUseCase;
		$this->removeTaskListUseCase = $removeTaskListUseCase;
		$this->updateTaskListUseCase = $updateTaskListUseCase;
		$this->getAllTaskListUseCase = $getAllTaskListUseCase;
		$this->logger = $logger;
	}

	/**
	 * @param Request $request
	 * @param string|null $id
	 *
	 * @return Response
	 */
	public function get(Request $request, string $id = null): Response
	{
		$data = [];
		try
		{
			if (is_null($id))
			{
				$collection = $this->getAllTaskListUseCase->execute();
				/** @var TaskList $taskList */
				foreach ($collection as $taskList)
				{
					$data[] = $this->transform($taskList);
				}
			}
			else
			{
				$taskList = $this->getListUseCase->execute(
					new TaskListId($request->get('id'))
				);

				$data = $this->transform($taskList);
			}
		}
		catch (TaskListNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task list ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to get List resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse(
			['data' => $data]
		);
	}

	/**
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function post(Request $request): Response
	{
		try
		{
			$id = $this->createTaskListUseCase->execute(
				$request->get('name')
			);

			$this->logger->info('Task list ' . $id->toString() . ' created.');
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to create List resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse(['id' => $id->toString()]);
	}

	/**
	 * @param Request $request
	 * @param string $id
	 *
	 * @return Response
	 */
	public function patch(Request $request, string $id): Response
	{
		try
		{
			$this->updateTaskListUseCase->execute(
				new TaskListId($id),
				$request->get('name')
			);

			$this->logger->info('Task list ' . $id . ' updated.');
		}
		catch (TaskListNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task list ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to update List resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse([], 204);
	}

	/**
	 * @param Request $request
	 * @param string $id
	 *
	 * @return Response
	 */
	public function delete(Request $request, string $id): Response
	{
		try
		{
			$this->removeTaskListUseCase->execute(new TaskListId($id));

			$this->logger->info('Task list ' . $id . ' deleted.');
		}
		catch (TaskListNotFoundException $e)
		{
			return new JsonResponse(
				[
					'error' => 'Task list ' . $request->get('id') . ' could not be found.'
				],
				404
			);
		}
		catch (\Throwable $e)
		{
			$this->logger->error(
				'Attempt to delete List resource has failed with error ' . $e->getMessage() . '.'
			);

			return new JsonResponse(['error' => 'Internal server error'], 500);
		}

		return new JsonResponse([], 204);
	}

	/**
	 * @param TaskList $taskList
	 *
	 * @return array<string, mixed>
	 */
	private function transform(TaskList $taskList): array
	{
		return [
			'id' => $taskList->getId()->toString(),
			'name' => $taskList->getName(),
			'createdAt' => $taskList->getCreatedAt()->format('Y-m-d H:i:s')
		];
	}
}
