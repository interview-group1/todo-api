<?php

declare(strict_types = 1);

namespace Interview\Todo\Infrastructure;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryException;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PdoTaskFactory
{
	/**
	 * @var OptionsResolver
	 */
	private OptionsResolver $resolver;

	/**
	 * @param OptionsResolver $resolver
	 */
	public function __construct(OptionsResolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param array<mixed> $row
	 *
	 * @return Task
	 * @throws \Exception
	 */
	public function create(array $row): Task
	{
		$this->resolveResultRow($row, ['id', 'name', 'created_at', 'removed_at', 'status', 'list_id']);

		return new Task(
			new TaskId($row['id']),
			$row['name'],
			new TaskListId($row['list_id']),
			new TaskStatus($row['status']),
			new \DateTimeImmutable($row['created_at']),
			!is_null($row['removed_at'])
				? new \DateTimeImmutable($row['removed_at'])
				: null,
		);
	}

	/**
	 * @param array<string> $row
	 * @param array<string> $requiredKeys
	 */
	private function resolveResultRow(array $row, array $requiredKeys): void
	{
		$this->resolver->setRequired($requiredKeys);

		try
		{
			$this->resolver->resolve($row);
		}
		catch (\InvalidArgumentException $e)
		{
			throw new TaskRepositoryException(
				'Mandatory fields are missing ' . json_encode($requiredKeys),
				0,
				$e
			);
		}
	}
}