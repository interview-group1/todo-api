<?php

declare(strict_types = 1);

namespace Interview\Todo\Infrastructure;

use Interview\Todo\Domain\UuidGeneratorInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class RamseyUuidGenerator implements UuidGeneratorInterface
{
	/**
	 * @inheritDoc
	 */
	public function generate(): UuidInterface
	{
		return Uuid::uuid6();
	}
}