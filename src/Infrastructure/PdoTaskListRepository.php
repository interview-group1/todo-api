<?php

declare(strict_types = 1);

namespace Interview\Todo\Infrastructure;

use Interview\Todo\Domain\TaskList\TaskListCollection;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListNotFoundException;
use Interview\Todo\Domain\TaskList\TaskListRepositoryException;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskList;
use PDO;
use PDOStatement;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PdoTaskListRepository implements TaskListRepositoryInterface
{
	private PDO $pdo;

	/**
	 * @var OptionsResolver
	 */
	private OptionsResolver $resolver;

	/**
	 * @param PDO $pdo
	 * @param OptionsResolver $resolver
	 */
	public function __construct(PDO $pdo, OptionsResolver $resolver) {
		$this->pdo = $pdo;
		$this->resolver = $resolver;
	}

	/**
	 * @inheritDoc
	 */
	public function getById(TaskListId $id): TaskList
	{
		$stmt = $this->executeQuery(
			'SELECT * FROM lists WHERE id = :id AND removed_at IS NULL',
			[':id' => $id->toString()]
		);

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row === false)
		{
			throw new TaskListNotFoundException(
				'Task list with identifier ' . $id->toString() . ' could not be found.'
			);
		}

		$this->resolveResultRow($row, ['id', 'name', 'created_at', 'removed_at']);

		return $this->createTaskList($row);
	}

	/**
	 * @inheritDoc
	 */
	public function getAll(): TaskListCollection
	{
		// TODO strankovani, filtrovani
		$stmt = $this->executeQuery(
			'SELECT * FROM lists WHERE removed_at IS NULL',
			[]
		);

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$collection = new TaskListCollection();
		foreach ($rows as $row)
		{
			$this->resolveResultRow($row, ['id', 'name', 'created_at', 'removed_at']);

			$collection->add(
				$this->createTaskList($row)
			);
		}

		return $collection;
	}

	/**
	 * @inheritDoc
	 */
	public function create(TaskList $taskList): void
	{
		$currentDateTime = new \DateTimeImmutable();
		$this->executeQuery(
			'INSERT INTO lists (id, name, created_at) VALUES (:id, :name, :createdAt)',
			[
				':id' => $taskList->getId()->toString(),
				':name' => $taskList->getName(),
				':createdAt' => $currentDateTime->format('Y-m-d H:i:s'),
			]
		);
	}

	/**
	 * @inheritDoc
	 */
	public function update(TaskList $taskList): void
	{
		$this->executeQuery(
			'UPDATE lists
			SET name = :name, removed_at = :removedAt
			WHERE id = :id',
			[
				':id' => $taskList->getId()->toString(),
				':name' => $taskList->getName(),
				':removedAt' => !is_null($taskList->getRemovedAt())
					? $taskList->getRemovedAt()->format('Y-m-d H:i:s')
					: null
			]
		);
	}

	/**
	 * @param array<string> $row
	 * @param array<string> $requiredKeys
	 */
	private function resolveResultRow(array $row, array $requiredKeys): void
	{
		$this->resolver->setRequired($requiredKeys);

		try
		{
			$this->resolver->resolve($row);
		}
		catch (\InvalidArgumentException $e)
		{
			throw new TaskListRepositoryException(
				'Mandatory fields are missing ' . json_encode($requiredKeys),
				0,
				$e
			);
		}
	}

	/**
	 * @param string $sql
	 * @param array<string, mixed> $bindings
	 *
	 * @return PDOStatement
	 * @throws TaskListRepositoryException
	 */
	private function executeQuery(string $sql, array $bindings): PDOStatement
	{
		$stmt = $this->pdo->prepare($sql);
		if ($stmt === false)
		{
			throw new TaskListRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true)
			);
		}

		try
		{
			$result = $stmt->execute($bindings);
		}
		catch (\Exception $e)
		{
			throw new TaskListRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true) .
				'. Got error "' . $e->getMessage() . '".',
				0,
				$e
			);
		}

		if ($result === false)
		{
			throw new TaskListRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true)
			);
		}

		return $stmt;
	}

	/**
	 * @param mixed $row
	 *
	 * @return TaskList
	 * @throws \Exception
	 */
	private function createTaskList(mixed $row): TaskList
	{
		return new TaskList(
			new TaskListId($row['id']),
			$row['name'],
			new \DateTimeImmutable($row['created_at']),
			!is_null($row['removed_at'])
				? new \DateTimeImmutable($row['removed_at'])
				: null
		);
	}
}