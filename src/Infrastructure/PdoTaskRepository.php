<?php

declare(strict_types = 1);

namespace Interview\Todo\Infrastructure;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskCollection;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskNotFoundException;
use Interview\Todo\Domain\Task\TaskRepositoryException;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskListId;
use PDO;
use PDOStatement;

class PdoTaskRepository implements TaskRepositoryInterface
{
	private PDO $pdo;

	/**
	 * @var PdoTaskFactory
	 */
	private PdoTaskFactory $factory;

	/**
	 * @param PDO $pdo
	 * @param PdoTaskFactory $factory
	 */
	public function __construct(PDO $pdo, PdoTaskFactory $factory)
	{
		$this->pdo = $pdo;
		$this->factory = $factory;
	}

	/**
	 * @inheritDoc
	 */
	public function getById(TaskListId $taskListId, TaskId $id): Task
	{
		$stmt = $this->executeQuery(
			'SELECT * FROM tasks WHERE id = :id AND removed_at IS NULL AND list_id = :listId',
			[
				':id' => $id->toString(),
				':listId' => $taskListId->toString()
			]
		);

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row === false)
		{
			throw new TaskNotFoundException(
				'Task with identifier ' . $id->toString() . ' could not be found.'
			);
		}

		return $this->factory->create($row);
	}

	/**
	 * @inheritDoc
	 */
	public function getAll(TaskListId $taskListId): TaskCollection
	{
		// TODO strankovani, filtrovani
		$stmt = $this->executeQuery(
			'SELECT * FROM tasks WHERE removed_at IS NULL AND list_id = :listId',
			[
				':listId' => $taskListId->toString()
			]
		);

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$collection = new TaskCollection();
		foreach ($rows as $row)
		{
			$collection->add(
				$this->factory->create($row)
			);
		}

		return $collection;
	}

	/**
	 * @inheritDoc
	 */
	public function create(Task $task): void
	{
		$currentDateTime = new \DateTimeImmutable();
		$this->executeQuery(
			'INSERT INTO tasks (id, name, created_at, status, list_id) 
				VALUES (:id, :name, :createdAt, :status, :listId)',
			[
				':id' => $task->getId()->toString(),
				':name' => $task->getName(),
				':status' => $task->getStatus()->toString(),
				':createdAt' => $currentDateTime->format('Y-m-d H:i:s'),
				':listId' => $task->getTaskListId()->toString()
			]
		);
	}

	/**
	 * @inheritDoc
	 */
	public function update(Task $task): void
	{
		$this->executeQuery(
			'UPDATE tasks
			SET name = :name, removed_at = :removedAt, status = :status
			WHERE id = :id',
			[
				':id' => $task->getId()->toString(),
				':name' => $task->getName(),
				':status' => $task->getStatus()->toString(),
				':removedAt' => !is_null($task->getRemovedAt())
					? $task->getRemovedAt()->format('Y-m-d H:i:s')
					: null
			]
		);
	}

	/**
	 * @param string $sql
	 * @param array<string, mixed> $bindings
	 *
	 * @return PDOStatement
	 * @throws TaskRepositoryException
	 */
	private function executeQuery(string $sql, array $bindings): PDOStatement
	{
		$stmt = $this->pdo->prepare($sql);
		if ($stmt === false)
		{
			throw new TaskRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true)
			);
		}

		try
		{
			$result = $stmt->execute($bindings);
		}
		catch (\Exception $e)
		{
			throw new TaskRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true) .
				'. Got error "' . $e->getMessage() . '".',
				0,
				$e
			);
		}

		if ($result === false)
		{
			throw new TaskRepositoryException(
				'Execution of query `' . $sql . '` has failed. Bindings: ' . var_export($bindings, true)
			);
		}

		return $stmt;
	}
}