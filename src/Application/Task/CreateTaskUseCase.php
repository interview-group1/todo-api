<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\Task;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\UuidGeneratorInterface;

class CreateTaskUseCase
{
	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @var UuidGeneratorInterface
	 */
	private UuidGeneratorInterface $uuidGenerator;

	/**
	 * @param TaskRepositoryInterface $taskRepository
	 * @param UuidGeneratorInterface $uuidGenerator
	 */
	public function __construct(
		TaskRepositoryInterface $taskRepository,
		UuidGeneratorInterface $uuidGenerator
	) {
		$this->taskRepository = $taskRepository;
		$this->uuidGenerator = $uuidGenerator;
	}

	/**
	 * @param TaskListId $taskListId
	 * @param string $name
	 *
	 * @return TaskId
	 */
	public function execute(TaskListId $taskListId, string $name): TaskId
	{
		$id = TaskId::fromUuid($this->uuidGenerator->generate());

		$this->taskRepository->create(
			new Task(
				$id,
				$name,
				$taskListId,
				new TaskStatus(TaskStatus::CREATED_STATUS),
				new \DateTimeImmutable()
			)
		);

		return $id;
	}
}