<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\Task;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskCollection;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;

class GetAllTasksUseCase
{
	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @param TaskRepositoryInterface $taskRepository
	 */
	public function __construct(
		TaskRepositoryInterface $taskRepository
	) {
		$this->taskRepository = $taskRepository;
	}

	/**
	 * @param TaskListId $taskListId
	 *
	 * @return TaskCollection<Task>
	 */
	public function execute(TaskListId $taskListId): TaskCollection
	{
		return $this->taskRepository->getAll($taskListId);
	}
}
