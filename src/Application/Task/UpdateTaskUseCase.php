<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\Task;

use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;

class UpdateTaskUseCase
{
	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @param TaskRepositoryInterface $taskRepository
	 */
	public function __construct(
		TaskRepositoryInterface $taskRepository
	) {
		$this->taskRepository = $taskRepository;
	}

	/**
	 * @param TaskListId $taskListId
	 * @param TaskId $id
	 * @param string|null $name
	 * @param TaskStatus|null $status
	 *
	 * @return void
	 */
	public function execute(
		TaskListId $taskListId,
		TaskId $id,
		string $name = null,
		TaskStatus $status = null
	): void {
		$task = $this->taskRepository->getById($taskListId, $id);

		if (!is_null($name))
		{
			$task->changeName($name);
		}

		if (!is_null($status))
		{
			$task->setStatus($status);
		}

		$this->taskRepository->update($task);
	}
}