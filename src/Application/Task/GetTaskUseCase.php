<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\Task;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskListId;

class GetTaskUseCase
{
	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @param TaskRepositoryInterface $taskRepository
	 */
	public function __construct(
		TaskRepositoryInterface $taskRepository
	) {
		$this->taskRepository = $taskRepository;
	}

	/**
	 * @param TaskListId $taskListId
	 * @param TaskId $id
	 *
	 * @return Task
	 */
	public function execute(TaskListId $taskListId, TaskId $id): Task
	{
		return $this->taskRepository->getById($taskListId, $id);
	}
}
