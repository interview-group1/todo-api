<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\Task;

use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskListId;

class RemoveTaskUseCase
{
	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @param TaskRepositoryInterface $taskRepository
	 */
	public function __construct(
		TaskRepositoryInterface $taskRepository
	) {
		$this->taskRepository = $taskRepository;
	}

	/**
	 * @param TaskListId $id
	 * @param TaskId $taskId
	 *
	 * @return void
	 */
	public function execute(TaskListId $id, TaskId $taskId): void
	{
		$task = $this->taskRepository->getById($id, $taskId);
		$task->setAsRemoved(new \DateTimeImmutable());

		$this->taskRepository->update($task);
	}
}