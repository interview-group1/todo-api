<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\TaskList;

use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;

class RemoveTaskListUseCase
{
	/**
	 * @var TaskListRepositoryInterface
	 */
	private TaskListRepositoryInterface $listRepository;

	/**
	 * @var TaskRepositoryInterface
	 */
	private TaskRepositoryInterface $taskRepository;

	/**
	 * @param TaskListRepositoryInterface $listRepository
	 * @param TaskRepositoryInterface $taskRepository
	 */
	public function __construct(
		TaskListRepositoryInterface $listRepository,
		TaskRepositoryInterface $taskRepository
	) {
		$this->listRepository = $listRepository;
		$this->taskRepository = $taskRepository;
	}

	/**
	 * @param TaskListId $taskListId
	 *
	 * @return void
	 */
	public function execute(TaskListId $taskListId): void
	{
		//TODO transakce
		$removedAt = new \DateTimeImmutable();

		$list = $this->listRepository->getById($taskListId);
		$list->setAsRemoved($removedAt);

		$this->listRepository->update($list);

		$tasks = $this->taskRepository->getAll($taskListId);
		foreach ($tasks as $task)
		{
			$task->setAsRemoved($removedAt);
			$this->taskRepository->update($task);
		}
	}
}