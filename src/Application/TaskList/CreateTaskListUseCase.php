<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\TaskList;

use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;
use Interview\Todo\Domain\UuidGeneratorInterface;

class CreateTaskListUseCase
{
	/**
	 * @var TaskListRepositoryInterface
	 */
	private TaskListRepositoryInterface $listRepository;

	/**
	 * @var UuidGeneratorInterface
	 */
	private UuidGeneratorInterface $uuidGenerator;

	/**
	 * @param TaskListRepositoryInterface $listRepository
	 * @param UuidGeneratorInterface $uuidGenerator
	 */
	public function __construct(
		TaskListRepositoryInterface $listRepository,
		UuidGeneratorInterface $uuidGenerator
	) {
		$this->listRepository = $listRepository;
		$this->uuidGenerator = $uuidGenerator;
	}

	/**
	 * @param string $name
	 *
	 * @return TaskListId
	 */
	public function execute(string $name): TaskListId
	{
		$id = TaskListId::fromUuid($this->uuidGenerator->generate());

		$this->listRepository->create(
			new TaskList(
				$id,
				$name,
				new \DateTimeImmutable() // i pro datum by se hodil vlastni domenovy objekt
			)
		);

		return $id;
	}
}