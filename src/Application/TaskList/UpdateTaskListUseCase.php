<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\TaskList;

use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;

class UpdateTaskListUseCase
{
	/**
	 * @var TaskListRepositoryInterface
	 */
	private TaskListRepositoryInterface $listRepository;

	/**
	 * @param TaskListRepositoryInterface $listRepository
	 */
	public function __construct(
		TaskListRepositoryInterface $listRepository
	) {
		$this->listRepository = $listRepository;
	}

	/**
	 * @param TaskListId $id
	 * @param string $name
	 *
	 * @return void
	 */
	public function execute(TaskListId $id, string $name): void
	{
		$taskList = $this->listRepository->getById($id);

		$taskList->changeName($name);

		$this->listRepository->update($taskList);
	}
}