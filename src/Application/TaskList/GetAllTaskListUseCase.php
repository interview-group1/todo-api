<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\TaskList;

use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListCollection;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;

class GetAllTaskListUseCase
{
	/**
	 * @var TaskListRepositoryInterface
	 */
	private TaskListRepositoryInterface $listRepository;

	/**
	 * @param TaskListRepositoryInterface $listRepository
	 */
	public function __construct(TaskListRepositoryInterface $listRepository)
	{
		$this->listRepository = $listRepository;
	}

	/**
	 * @return TaskListCollection<TaskList>
	 */
	public function execute(): TaskListCollection
	{
		return $this->listRepository->getAll();
	}
}
