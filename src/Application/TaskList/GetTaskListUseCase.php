<?php

declare(strict_types = 1);

namespace Interview\Todo\Application\TaskList;

use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;

class GetTaskListUseCase
{
	/**
	 * @var TaskListRepositoryInterface
	 */
	private TaskListRepositoryInterface $listRepository;

	/**
	 * @param TaskListRepositoryInterface $listRepository
	 */
	public function __construct(TaskListRepositoryInterface $listRepository)
	{
		$this->listRepository = $listRepository;
	}

	/**
	 * @param TaskListId $id
	 *
	 * @return TaskList
	 */
	public function execute(TaskListId $id): TaskList
	{
		return $this->listRepository->getById($id);
	}
}
