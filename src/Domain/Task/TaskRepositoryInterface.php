<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

use Interview\Todo\Domain\TaskList\TaskListId;

interface TaskRepositoryInterface
{
	/**
	 * @param TaskListId $taskListId
	 * @param TaskId $id
	 *
	 * @return Task
	 * @throws TaskNotFoundException
	 * @throws TaskRepositoryException
	 */
	public function getById(TaskListId $taskListId, TaskId $id): Task;

	/**
	 * @param TaskListId $taskListId
	 *
	 * @return TaskCollection<Task>
	 * @throws TaskRepositoryException
	 */
	public function getAll(TaskListId $taskListId): TaskCollection;

	/**
	 * @param Task $task
	 *
	 * @throws TaskRepositoryException
	 */
	public function create(Task $task): void;

	/**
	 * @param Task $task
	 *
	 * @throws TaskRepositoryException
	 */
	public function update(Task $task): void;
}