<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

use Interview\Todo\Domain\TaskList\TaskListId;

class Task
{
	private TaskId $id;

	private string $name;

	private TaskListId $taskListId;

	private TaskStatus $status;

	private \DateTimeImmutable $createdAt;

	private ?\DateTimeImmutable $removedAt;

	/**
	 * @param TaskId $id
	 * @param string $name
	 * @param TaskListId $taskListId
	 * @param TaskStatus $status
	 * @param \DateTimeImmutable $createdAt
	 * @param \DateTimeImmutable|null $removedAt
	 */
	public function __construct(
		TaskId $id,
		string $name,
		TaskListId $taskListId,
		TaskStatus $status,
		\DateTimeImmutable $createdAt,
		\DateTimeImmutable $removedAt = null
	) {
		$this->id = $id;
		$this->name = $name;
		$this->taskListId = $taskListId;
		$this->status = $status;
		$this->createdAt = $createdAt;
		$this->removedAt = $removedAt;
	}

	/**
	 * @return TaskId
	 */
	public function getId(): TaskId
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return void
	 */
	public function changeName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return TaskStatus
	 */
	public function getStatus(): TaskStatus
	{
		return $this->status;
	}

	/**
	 * @param TaskStatus $status
	 */
	public function setStatus(TaskStatus $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return TaskListId
	 */
	public function getTaskListId(): TaskListId
	{
		return $this->taskListId;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreatedAt(): \DateTimeImmutable
	{
		return $this->createdAt;
	}

	/**
	 * @return \DateTimeImmutable|null
	 */
	public function getRemovedAt(): ?\DateTimeImmutable
	{
		return $this->removedAt;
	}

	/**
	 * @param \DateTimeImmutable $removedAt
	 */
	public function setAsRemoved(\DateTimeImmutable $removedAt): void
	{
		$this->removedAt = $removedAt;
	}

	/**
	 * @return bool
	 */
	public function isRemoved(): bool
	{
		return is_null($this->removedAt);
	}
}