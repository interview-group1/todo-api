<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

class InvalidTaskStatusException extends \RuntimeException
{
	/**
	 * @param string $status
	 * @param array<string> $availableStatuses
	 */
	public function __construct(string $status, array $availableStatuses)
	{
		parent::__construct(
			sprintf(
				'Unexpected task status %s. Allowed are %s.',
				$status,
				implode(',', $availableStatuses)
			)
		);
	}
}