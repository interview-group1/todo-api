<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

class TaskRepositoryException extends \RuntimeException
{

}