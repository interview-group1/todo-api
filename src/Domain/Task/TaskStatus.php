<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

class TaskStatus
{
	const CREATED_STATUS = 'CREATED';
	const COMPLETED_STATUS = 'COMPLETED';

	/**
	 * @var string[]
	 */
	private array $availableStatuses = [
		self::CREATED_STATUS,
		self::COMPLETED_STATUS
	];

	private string $status;

	/**
	 * @param string $status
	 */
	public function __construct(string $status)
	{
		if (!in_array($status, $this->availableStatuses))
		{
			throw new InvalidTaskStatusException($status, $this->availableStatuses);
		}

		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function toString(): string
	{
		return $this->status;
	}
}