<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

use Doctrine\Common\Collections\ArrayCollection;

class TaskCollection extends ArrayCollection
{

}