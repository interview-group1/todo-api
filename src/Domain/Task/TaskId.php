<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\Task;

use Ramsey\Uuid\UuidInterface;

class TaskId
{
	public string $id;

	/**
	 * @param string $id
	 */
	public function __construct(string $id)
	{
		$this->id = $id;
	}

	/**
	 * @param UuidInterface $uuid
	 *
	 * @return TaskId
	 */
	public static function fromUuid(UuidInterface $uuid): TaskId
	{
		return new self($uuid->toString());
	}

	/**
	 * @return string
	 */
	public function toString(): string
	{
		return $this->id;
	}
}