<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain;

use Ramsey\Uuid\UuidInterface;

interface UuidGeneratorInterface
{
	/**
	 * @return UuidInterface
	 */
	public function generate(): UuidInterface;
}