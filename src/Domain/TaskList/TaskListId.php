<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

use Ramsey\Uuid\UuidInterface;

class TaskListId
{
	public string $id;

	/**
	 * @param string $id
	 */
	public function __construct(string $id)
	{
		$this->id = $id;
	}

	/**
	 * @param UuidInterface $uuid
	 *
	 * @return TaskListId
	 */
	public static function fromUuid(UuidInterface $uuid): TaskListId
	{
		return new self($uuid->toString());
	}

	/**
	 * @return string
	 */
	public function toString(): string
	{
		return $this->id;
	}
}