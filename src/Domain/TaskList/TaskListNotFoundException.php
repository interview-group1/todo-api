<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

class TaskListNotFoundException extends \RuntimeException
{

}