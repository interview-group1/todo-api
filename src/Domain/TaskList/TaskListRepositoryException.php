<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

class TaskListRepositoryException extends \RuntimeException
{

}