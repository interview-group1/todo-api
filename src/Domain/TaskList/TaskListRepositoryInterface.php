<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

interface TaskListRepositoryInterface
{
	/**
	 * @param TaskListId $id
	 *
	 * @return TaskList
	 * @throws TaskListNotFoundException
	 * @throws TaskListRepositoryException
	 */
	public function getById(TaskListId $id): TaskList;

	/**
	 * @return TaskListCollection<TaskList>
	 * @throws TaskListRepositoryException
	 */
	public function getAll(): TaskListCollection;

	/**
	 * @param TaskList $taskList
	 * @throws TaskListRepositoryException
	 */
	public function create(TaskList $taskList): void;

	/**
	 * @param TaskList $taskList
	 * @throws TaskListRepositoryException
	 */
	public function update(TaskList $taskList): void;
}