<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

class TaskList
{
	private TaskListId $id;

	private string $name;

	private \DateTimeImmutable $createdAt;

	private ?\DateTimeImmutable $removedAt;

	/**
	 * @param TaskListId $id
	 * @param string $name
	 * @param \DateTimeImmutable $createdAt
	 * @param \DateTimeImmutable|null $removedAt
	 */
	public function __construct(
		TaskListId $id,
		string $name,
		\DateTimeImmutable $createdAt,
		\DateTimeImmutable $removedAt = null
	) {
		$this->id = $id;
		$this->name = $name;
		$this->createdAt = $createdAt;
		$this->removedAt = $removedAt;
	}

	/**
	 * @return TaskListId
	 */
	public function getId(): TaskListId
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return void
	 */
	public function changeName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreatedAt(): \DateTimeImmutable
	{
		return $this->createdAt;
	}

	/**
	 * @return \DateTimeImmutable|null
	 */
	public function getRemovedAt(): ?\DateTimeImmutable
	{
		return $this->removedAt;
	}

	/**
	 * @param \DateTimeImmutable $removedAt
	 */
	public function setAsRemoved(\DateTimeImmutable $removedAt): void
	{
		$this->removedAt = $removedAt;
	}

	/**
	 * @return bool
	 */
	public function isRemoved(): bool
	{
		return is_null($this->removedAt);
	}
}