<?php

declare(strict_types = 1);

namespace Interview\Todo\Domain\TaskList;

use Doctrine\Common\Collections\ArrayCollection;

class TaskListCollection extends ArrayCollection
{

}