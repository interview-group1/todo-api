USE `interview`;

DROP TABLE IF EXISTS `lists`;
CREATE TABLE `lists` (
						 `id` varchar(50) NOT NULL,
						 `name` varchar(150) NOT NULL,
						 `created_at` datetime NOT NULL,
						 `removed_at` datetime DEFAULT NULL,
						 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
						 `id` varchar(50) NOT NULL,
						 `name` varchar(150) NOT NULL,
						 `created_at` datetime NOT NULL,
						 `removed_at` datetime DEFAULT NULL,
						 `status` enum('CREATED','COMPLETED') NOT NULL,
						 `list_id` varchar(50) NOT NULL,
						 PRIMARY KEY (`id`),
						 KEY `list_id` (`list_id`),
						 CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `lists` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;