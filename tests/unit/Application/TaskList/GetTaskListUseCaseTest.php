<?php

namespace Interview\Todo\tests\unit\Application\TaskList;

use Interview\Todo\Application\TaskList\CreateTaskListUseCase;
use Interview\Todo\Application\TaskList\GetTaskListUseCase;
use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetTaskListUseCaseTest extends TestCase
{
	/**
	 * @var TaskListRepositoryInterface&MockObject
	 */
	private MockObject|TaskListRepositoryInterface $repository;

	public function setUp(): void
	{
		$this->repository = $this->createMock(TaskListRepositoryInterface::class);
	}

	/**
	 * @test
	 */
	public function it_will_get_required_task_list(): void
	{
		$taskList = new TaskList(
			new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
			'test name',
			new \DateTimeImmutable()
		);

		$this->repository
			->expects($this->exactly(1))
			->method('getById')
			->willReturn($taskList);

		$useCase = new GetTaskListUseCase(
			$this->repository
		);

		$this->assertEquals(
			$taskList,
			$useCase->execute(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004')
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_ignore_any_exception(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('getById')
			->willThrowException(new \Exception());

		$useCase = new GetTaskListUseCase(
			$this->repository
		);

		$this->expectException(\Exception::class);
		$useCase->execute(new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'));
	}
}
