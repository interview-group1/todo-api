<?php

namespace Interview\Todo\tests\unit\Application\TaskList;

use Interview\Todo\Application\TaskList\GetAllTaskListUseCase;
use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListCollection;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetAllTaskListUseCaseTest extends TestCase
{
	/**
	 * @var TaskListRepositoryInterface&MockObject
	 */
	private MockObject|TaskListRepositoryInterface $repository;

	public function setUp(): void
	{
		$this->repository = $this->createMock(TaskListRepositoryInterface::class);
	}

	/**
	 * @test
	 */
	public function it_will_get_all_tasks_list(): void
	{
		$taskList = new TaskList(
			new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
			'test name',
			new \DateTimeImmutable()
		);
		$collection = new TaskListCollection([$taskList]);
		$this->repository
			->expects($this->exactly(1))
			->method('getAll')
			->willReturn($collection);

		$useCase = new GetAllTaskListUseCase(
			$this->repository
		);

		$this->assertEquals(
			$collection,
			$useCase->execute()
		);
	}

	/**
	 * @test
	 */
	public function it_will_ignore_any_exception(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('getAll')
			->willThrowException(new \Exception());

		$useCase = new GetAllTaskListUseCase(
			$this->repository
		);

		$this->expectException(\Exception::class);
		$useCase->execute();
	}
}
