<?php

namespace Interview\Todo\tests\unit\Application\TaskList;

use Interview\Todo\Application\TaskList\CreateTaskListUseCase;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryInterface;
use Interview\Todo\Domain\UuidGeneratorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CreateTaskListUseCaseTest extends TestCase
{
	/**
	 * @var TaskListRepositoryInterface&MockObject
	 */
	private MockObject|TaskListRepositoryInterface $repository;

	/**
	 * @var UuidGeneratorInterface&MockObject
	 */
	private UuidGeneratorInterface|MockObject $uuidGenerator;

	public function setUp(): void
	{
		$this->repository = $this->createMock(TaskListRepositoryInterface::class);
		$this->uuidGenerator = $this->createMock(UuidGeneratorInterface::class);
	}

	/**
	 * @test
	 */
	public function it_will_create_task_list(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('create');

		$uuid = Uuid::uuid6();
		$this->uuidGenerator
			->expects($this->exactly(1))
			->method('generate')
			->willReturn($uuid);

		$useCase = new CreateTaskListUseCase(
			$this->repository,
			$this->uuidGenerator
		);

		$this->assertEquals(
			TaskListId::fromUuid($uuid),
			$useCase->execute('test name')
		);
	}

	/**
	 * @test
	 */
	public function it_will_ignore_any_exception(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('create')
			->willThrowException(new \Exception());

		$uuid = Uuid::uuid6();
		$this->uuidGenerator
			->expects($this->exactly(1))
			->method('generate')
			->willReturn($uuid);

		$useCase = new CreateTaskListUseCase(
			$this->repository,
			$this->uuidGenerator
		);

		$this->expectException(\Exception::class);
		$useCase->execute('test name');
	}
}
