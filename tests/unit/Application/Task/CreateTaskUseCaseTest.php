<?php

namespace Interview\Todo\tests\unit\Application\Task;

use Interview\Todo\Application\Task\CreateTaskUseCase;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\UuidGeneratorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CreateTaskUseCaseTest extends TestCase
{
	/**
	 * @var TaskRepositoryInterface&MockObject
	 */
	private MockObject|TaskRepositoryInterface $repository;

	/**
	 * @var UuidGeneratorInterface&MockObject
	 */
	private UuidGeneratorInterface|MockObject $uuidGenerator;

	public function setUp(): void
	{
		$this->repository = $this->createMock(TaskRepositoryInterface::class);
		$this->uuidGenerator = $this->createMock(UuidGeneratorInterface::class);
	}

	/**
	 * @test
	 */
	public function it_will_create_task(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('create');

		$uuid = Uuid::uuid6();
		$this->uuidGenerator
			->expects($this->exactly(1))
			->method('generate')
			->willReturn($uuid);

		$useCase = new CreateTaskUseCase(
			$this->repository,
			$this->uuidGenerator
		);

		$this->assertEquals(
			TaskId::fromUuid($uuid),
			$useCase->execute(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name'
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_ignore_any_exception(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('create')
			->willThrowException(new \Exception());

		$uuid = Uuid::uuid6();
		$this->uuidGenerator
			->expects($this->exactly(1))
			->method('generate')
			->willReturn($uuid);

		$useCase = new CreateTaskUseCase(
			$this->repository,
			$this->uuidGenerator
		);

		$this->expectException(\Exception::class);
		$useCase->execute(
			new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
			'test name'
		);
	}
}
