<?php

namespace Interview\Todo\tests\unit\Application\Task;

use Interview\Todo\Application\Task\GetTaskUseCase;
use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryInterface;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetTaskUseCaseTest extends TestCase
{
	/**
	 * @var TaskRepositoryInterface&MockObject
	 */
	private MockObject|TaskRepositoryInterface $repository;

	public function setUp(): void
	{
		$this->repository = $this->createMock(TaskRepositoryInterface::class);
	}

	/**
	 * @test
	 */
	public function it_will_get_required_task(): void
	{
		$task = new Task(
			new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
			'test name',
			new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004'),
			new TaskStatus('CREATED'),
			new \DateTimeImmutable()
		);

		$this->repository
			->expects($this->exactly(1))
			->method('getById')
			->willReturn($task);

		$useCase = new GetTaskUseCase(
			$this->repository
		);

		$this->assertEquals(
			$task,
			$useCase->execute(
				new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004'),
				new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004')
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_ignore_any_exception(): void
	{
		$this->repository
			->expects($this->exactly(1))
			->method('getById')
			->willThrowException(new \Exception());

		$useCase = new GetTaskUseCase(
			$this->repository
		);

		$this->expectException(\Exception::class);
		$useCase->execute(
			new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004'),
			new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004')
		);
	}
}
