<?php

namespace Interview\Todo\tests\unit\Infrastructure;

use Interview\Todo\Domain\Task\InvalidTaskStatusException;
use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryException;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Infrastructure\PdoTaskFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PdoTaskFactoryTest extends TestCase
{
	/**
	 * @test
	 */
	public function it_will_create_task_entity(): void
	{
		$factory = new PdoTaskFactory(new OptionsResolver());

		$this->assertEquals(
			new Task(
				new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name',
				new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004'),
				new TaskStatus('CREATED'),
				new \DateTimeImmutable('2021-03-28 12:12:12')
			),
			$factory->create(
				[
					'id' => '1eb8f155-5f84-6120-8bd6-0242c0a88004',
					'name' => 'test name',
					'status' => 'CREATED',
					'created_at' => '2021-03-28 12:12:12',
					'list_id' => 'aaa-5f84-6120-8bd6-0242c0a88004',
					'removed_at' => null
				]
			)
		);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_when_mandatory_key_is_missing(): void
	{
		$factory = new PdoTaskFactory(new OptionsResolver());

		$this->expectException(TaskRepositoryException::class);
		$factory->create(
			[
				'name' => 'test name',
				'status' => 'CREATED',
				'created_at' => '2021-03-28 12:12:12',
				'list_id' => 'aaa-5f84-6120-8bd6-0242c0a88004',
				'removed_at' => null
			]
		);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_when_task_status_is_not_valid(): void
	{
		$factory = new PdoTaskFactory(new OptionsResolver());

		$this->expectException(InvalidTaskStatusException::class);
		$factory->create(
			[
				'id' => '1eb8f155-5f84-6120-8bd6-0242c0a88004',
				'name' => 'test name',
				'status' => 'INVALID_STATUS',
				'created_at' => '2021-03-28 12:12:12',
				'list_id' => 'aaa-5f84-6120-8bd6-0242c0a88004',
				'removed_at' => null
			]
		);
	}
}
