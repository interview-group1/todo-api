<?php

namespace Interview\Todo\tests\unit\Infrastructure;

use Interview\Todo\Domain\TaskList\TaskList;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Domain\TaskList\TaskListRepositoryException;
use Interview\Todo\Infrastructure\PdoTaskListRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PdoTaskListRepositoryTest extends TestCase
{
	/**
	 * @var MockObject&\PDO
	 */
	private \PDO|MockObject $pdo;

	public function setUp(): void
	{
		$this->pdo = $this->createMock(\PDO::class);
	}

	/**
	 * @test
	 */
	public function it_will_return_task_list_by_id(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');
		$stmt
			->expects($this->exactly(1))
			->method('fetch')
			->willReturn(
				[
					'id' => '1eb8f155-5f84-6120-8bd6-0242c0a88004',
					'name' => 'test name',
					'created_at' => '2021-03-28 12:12:12',
					'removed_at' => null
				]
			);

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskListRepository(
			$this->pdo,
			new OptionsResolver()
		);

		$id = new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004');
		$this->assertEquals(
			new TaskList(
				$id,
				'test name',
				new \DateTimeImmutable('2021-03-28 12:12:12')
			),
			$repository->getById($id)
		);
	}

	/**
	 * @test
	 */
	public function it_will_create_task_list(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskListRepository(
			$this->pdo,
			new OptionsResolver()
		);

		$repository->create(
			new TaskList(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name',
				new \DateTimeImmutable('2021-03-28 12:12:12')
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_throw_exception_when_pdo_fails_during_creation(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute')
			->willThrowException(new \PDOException());

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskListRepository(
			$this->pdo,
			new OptionsResolver()
		);

		$this->expectException(TaskListRepositoryException::class);
		$repository->create(
			new TaskList(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name',
				new \DateTimeImmutable('2021-03-28 12:12:12')
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_update_task_list(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskListRepository(
			$this->pdo,
			new OptionsResolver()
		);

		$repository->update(
			new TaskList(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name',
				new \DateTimeImmutable('2021-03-28 12:12:12')
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_throw_exception_when_pdo_fails_during_udpate(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute')
			->willThrowException(new \PDOException());

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskListRepository(
			$this->pdo,
			new OptionsResolver()
		);

		$this->expectException(TaskListRepositoryException::class);
		$repository->update(
			new TaskList(
				new TaskListId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
				'test name',
				new \DateTimeImmutable('2021-03-28 12:12:12')
			)
		);
	}
}
