<?php

namespace Interview\Todo\tests\unit\Infrastructure;

use Interview\Todo\Domain\Task\Task;
use Interview\Todo\Domain\Task\TaskId;
use Interview\Todo\Domain\Task\TaskRepositoryException;
use Interview\Todo\Domain\Task\TaskStatus;
use Interview\Todo\Domain\TaskList\TaskListId;
use Interview\Todo\Infrastructure\PdoTaskFactory;
use Interview\Todo\Infrastructure\PdoTaskRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PdoTaskRepositoryTest extends TestCase
{
	/**
	 * @var MockObject&\PDO
	 */
	private \PDO|MockObject $pdo;

	public function setUp(): void
	{
		$this->pdo = $this->createMock(\PDO::class);
	}

	/**
	 * @test
	 */
	public function it_will_return_task_by_id(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');
		$stmt
			->expects($this->exactly(1))
			->method('fetch')
			->willReturn(
				[
					'id' => '1eb8f155-5f84-6120-8bd6-0242c0a88004',
					'name' => 'test name',
					'status' => 'CREATED',
					'created_at' => '2021-03-28 12:12:12',
					'list_id' => 'aaa-5f84-6120-8bd6-0242c0a88004',
					'removed_at' => null
				]
			);

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskRepository(
			$this->pdo,
			new PdoTaskFactory(
				new OptionsResolver()
			)
		);

		$listId = new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004');
		$taskId = new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004');

		$this->assertEquals(
			new Task(
				$taskId,
				'test name',
				$listId,
				new TaskStatus('CREATED'),
				new \DateTimeImmutable('2021-03-28 12:12:12')
			),
			$repository->getById(
				$listId,
				$taskId
			)
		);
	}

	/**
	 * @test
	 */
	public function it_will_create_task(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskRepository(
			$this->pdo,
			new PdoTaskFactory(
				new OptionsResolver()
			)
		);

		$repository->create(
			$this->getTaskObject()
		);
	}

	/**
	 * @test
	 */
	public function it_will_throw_exception_when_pdo_fails_during_creation(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute')
			->willThrowException(new \PDOException());

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskRepository(
			$this->pdo,
			new PdoTaskFactory(
				new OptionsResolver()
			)
		);

		$this->expectException(TaskRepositoryException::class);
		$repository->create(
			$this->getTaskObject()
		);
	}

	/**
	 * @test
	 */
	public function it_will_update_task(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute');

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskRepository(
			$this->pdo,
			new PdoTaskFactory(
				new OptionsResolver()
			)
		);

		$repository->update(
			$this->getTaskObject()
		);
	}

	/**
	 * @test
	 */
	public function it_will_throw_exception_when_pdo_fails_during_update(): void
	{
		$stmt = $this->createMock(\PDOStatement::class);

		$stmt
			->expects($this->exactly(1))
			->method('execute')
			->willThrowException(new \PDOException());

		$this->pdo
			->expects($this->exactly(1))
			->method('prepare')
			->willReturn($stmt);

		$repository = new PdoTaskRepository(
			$this->pdo,
			new PdoTaskFactory(
				new OptionsResolver()
			)
		);

		$this->expectException(TaskRepositoryException::class);
		$repository->update(
			$this->getTaskObject()
		);
	}

	/**
	 * @return Task
	 */
	private function getTaskObject(): Task
	{
		return new Task(
			new TaskId('1eb8f155-5f84-6120-8bd6-0242c0a88004'),
			'test name',
			new TaskListId('aaa-5f84-6120-8bd6-0242c0a88004'),
			new TaskStatus('CREATED'),
			new \DateTimeImmutable('2021-03-28 12:12:12')
		);
	}
}
